class AlbumsController < ApplicationController
  def index
    @albums = @page.albums.map{ |a| a if a.name.match('A K E R A') != nil }.compact!
    
  end

  def show
    @album = @page.albums.map{|x| x if x.identifier == "#{params[:id]}"}.compact![0]
    @photos = @album.photos(:limit => 200)
  end
end
