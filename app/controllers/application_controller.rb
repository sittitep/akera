class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate

  def authenticate
    app = FbGraph::Application.new(300450546746887, :secret => '71e6e5ab681e76090a2da7065905bb7a')
    @token = app.get_access_token
    @page = FbGraph::Page.fetch('byakera', :access_token => @token)
  end
end
