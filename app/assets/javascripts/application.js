// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require masonry.pkgd
//= require jquery.sticky-kit.min
//= require jquery.zoom.min
//= require equalize.min
//= require jquery-scrollto
//= require_tree .

$(window).load(function(){

  var $container = $('#gallery');
  // initialize
  $container.masonry({
    columnWidth: 300,
    itemSelector: '.item',
    isFitWidth: true
  });

  // $('.gallery').equalize('outerWidth');

  $('.close-item-detail').on('click', function(){
    $(".item-detail").slideUp(400);
  });

  $('.item.photo').on('click', function(){

     var newImg = $(this).find("img").attr("src")
     var newText = $(this).parent().find("p.photo-name").text()

    $(".item-detail").find("img").attr("src", newImg)
    $(".item-description").find("p").html(newText)

    $(".item-detail").hide()

    $(".gallery").ScrollTo({
      callback: function(){
        $(".item-detail").slideDown(400);
      }
    });
    
  });
});

$(window).resize(function(){
  // $('.gallery').equalize('outerWidth');
});

$(document).ready(function(){
  $('.header').sticky({
    wrapperClassName: "header-stick",
    getWidthFrom: '.header-stick'
  });

  $(".item-image").zoom();
  $('.banner').bxSlider({
    auto: true,
    controls: false
  });
});